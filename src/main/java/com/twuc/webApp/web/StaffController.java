package com.twuc.webApp.web;

import com.twuc.webApp.contract.reservation.ContractReservationRequest;
import com.twuc.webApp.contract.reservation.ContractReservationResponse;
import com.twuc.webApp.contract.reservation.ReservationStartTime;
import com.twuc.webApp.contract.reservation.ReservationTime;
import com.twuc.webApp.contract.staff.ContractStaffRequest;
import com.twuc.webApp.contract.staff.ContractStaffResponse;
import com.twuc.webApp.contract.zone.ContractZoneRequest;
import com.twuc.webApp.domain.reservation.model.Reservation;
import com.twuc.webApp.domain.staff.StaffRepository;
import com.twuc.webApp.domain.staff.model.Staff;
import com.twuc.webApp.domain.zone.Zone;
import com.twuc.webApp.exception.ReservationConflict;
import com.twuc.webApp.exception.ReservationDurationInvalid;
import com.twuc.webApp.exception.ReservationNonWorkingTime;
import com.twuc.webApp.exception.ReservationTimeConflict;
import com.twuc.webApp.exception.ReservationTimeInvalid;
import com.twuc.webApp.exception.ZoneIdInvalidException;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/staffs")
public class StaffController {

    private final StaffRepository staffRepository;

    public StaffController(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }

    @PostMapping
    public ResponseEntity<Void> saveStaff(@RequestBody @Valid ContractStaffRequest contractStaffRequest) {
        Staff savedStaff = staffRepository.save(contractStaffRequest.toStaff());
        URI uri = linkTo(methodOn(StaffController.class).getStaffs(savedStaff.getId())).toUri();
        return ResponseEntity.created(uri).build();

    }

    @GetMapping("/{staffId}")
    public ResponseEntity<ContractStaffResponse> getStaffs(@PathVariable Long staffId) {
        Optional<Staff> optionalStaff = staffRepository.findById(staffId);
        if (optionalStaff.isPresent()) {
            ContractStaffResponse contractStaffResponse = new ContractStaffResponse(optionalStaff.get());
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(contractStaffResponse);
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping
    public ResponseEntity<List> findAll() {
        List<ContractStaffResponse> contractStaffResponses = new ArrayList<>();
        List<Staff> staffs = staffRepository.findAll(Sort.by("id"));
        staffs.forEach(staff -> contractStaffResponses.add(new ContractStaffResponse(staff)));
        return ResponseEntity.ok(contractStaffResponses);
    }

    @PutMapping("/{staffId}/timezone")
    public ResponseEntity<Void> putStaff(@PathVariable Long staffId,
                                         @RequestBody @Valid ContractZoneRequest contractZoneRequest) {
        checkZoneId(contractZoneRequest.getZoneId());
        Optional<Staff> optionalStaff = staffRepository.findById(staffId);
        if (optionalStaff.isPresent()) {
            Staff staff= optionalStaff.get();
            staff.setZone(contractZoneRequest.toZone());
            staffRepository.save(staff);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    private void checkZoneId(String zoneId) {
        Set<String> zoneIds = ZoneRulesProvider.getAvailableZoneIds();
        if (!zoneIds.contains(zoneId)) {
            throw new ZoneIdInvalidException("zone id is invalid");
        }
    }

    @PostMapping("/{staffId}/reservations")
    public ResponseEntity<Void> reservation(@PathVariable Long staffId,
                                      @RequestBody @Valid ContractReservationRequest contractReservationRequest) {
        URI uri = linkTo(methodOn(StaffController.class).findAllReservations(staffId)).toUri();
        Optional<Staff> optionalStaff = staffRepository.findById(staffId);
        if (optionalStaff.isPresent()) {
            Staff rob = optionalStaff.get();
            if (rob.getZone() == null) {
                String message = String.format("{%s %s} is not qualified.", rob.getFirstName(), rob.getLastName());
                throw new ReservationConflict(message);
            }
            ZonedDateTime robStartTime = ZonedDateTime.ofInstant(contractReservationRequest.getStartTime().toInstant(), ZoneId.of(rob.getZone().getZoneId()));

            if (robStartTime.minusDays(2).isBefore(ZonedDateTime.now(ZoneId.of(rob.getZone().getZoneId())))) {
                throw new ReservationTimeInvalid("Invalid time: the application is too close from now. The interval should be greater than 48 hours.");
            }
            long reservationDuration = getDuration(contractReservationRequest.getDuration());
            if (reservationDuration > 3 || reservationDuration < 1) {
                throw new ReservationDurationInvalid("The application duration should be within 1-3 hours.");
            }
            ZonedDateTime robEndTime = robStartTime.plusHours(getDuration(contractReservationRequest.getDuration()));
            if (rob.getReservations().size() > 0) {
                rob.getReservations().forEach(reservation -> {
                    ZonedDateTime startTime = reservation.getStartTime().atZone(ZoneId.of(rob.getZone().getZoneId()));
                    ZonedDateTime endTime = startTime.plusHours(reservation.getDuration());

                    if (!(robEndTime.isBefore(endTime)|| robStartTime.isAfter(startTime))) {
                        throw new ReservationTimeConflict("The application is conflict with existing application.");
                    }
                });
            }

            if (robStartTime.getHour() < 9 || robEndTime.getHour() > 17) {
                throw new ReservationNonWorkingTime("You know, our staff has their own life.");
            }

            Reservation reservation = contractReservationRequest.toReservation();
            reservation.setStaff(rob);
            rob.getReservations().add(reservation);
            staffRepository.save(rob);
            return ResponseEntity.created(uri).build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{staffId}/reservations")
    public ResponseEntity<List> findAllReservations(@PathVariable Long staffId) {
        List<ContractReservationResponse> contractReservationResponses = new ArrayList<>();
        Optional<Staff> optionalStaff = staffRepository.findById(staffId);
        if (optionalStaff.isPresent()) {
            Staff savedStaff = optionalStaff.get();
            List<Reservation> reservations = savedStaff.getReservations().stream().sorted(Comparator.comparing(Reservation::getStartTime)).collect(Collectors.toList());
            reservations.forEach(reservation -> {
                ContractReservationResponse contractReservationResponse = new ContractReservationResponse(reservation);
                ReservationTime client = new ReservationTime(reservation.getZoneId(), reservation.getStartTime());
                ReservationTime staff = new ReservationTime(reservation.getStaff().getZone().getZoneId(), reservation.getStartTime());
                contractReservationResponse.setStartTime(new ReservationStartTime(client, staff));
                contractReservationResponses.add(contractReservationResponse);
            });
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(contractReservationResponses);
        }
        return ResponseEntity.notFound().build();
    }

    private long getDuration(Duration duration) {
        return duration.toHours();
    }
}
