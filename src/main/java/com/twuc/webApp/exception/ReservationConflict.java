package com.twuc.webApp.exception;

public class ReservationConflict extends RuntimeException{
    public ReservationConflict(String message) {
        super(message);
    }
}
