package com.twuc.webApp.exception;

public class ReservationDurationInvalid extends RuntimeException {
    public ReservationDurationInvalid(String message) {
        super(message);
    }
}
