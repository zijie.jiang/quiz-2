package com.twuc.webApp.exception;

public class ReservationNonWorkingTime extends RuntimeException {
    public ReservationNonWorkingTime(String message) {
        super(message);
    }
}
