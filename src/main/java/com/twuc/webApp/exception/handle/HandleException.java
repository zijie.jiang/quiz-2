package com.twuc.webApp.exception.handle;

import com.twuc.webApp.exception.ReservationConflict;
import com.twuc.webApp.exception.ReservationDurationInvalid;
import com.twuc.webApp.exception.ReservationNonWorkingTime;
import com.twuc.webApp.exception.ReservationTimeConflict;
import com.twuc.webApp.exception.ReservationTimeInvalid;
import com.twuc.webApp.domain.message.Message;
import com.twuc.webApp.exception.ZoneIdInvalidException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandleException {

    @ExceptionHandler(value = ReservationConflict.class)
    public ResponseEntity<Message> handleConflictException(ReservationConflict exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(new Message(exception.getMessage()));
    }

    @ExceptionHandler(value = ReservationTimeInvalid.class)
    public ResponseEntity<Message> handleTimeInvalid(ReservationTimeInvalid exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new Message(exception.getMessage()));
    }

    @ExceptionHandler(value = ReservationTimeConflict.class)
    public ResponseEntity<Message> handleConflictTime(ReservationTimeConflict exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(new Message(exception.getMessage()));
    }

    @ExceptionHandler(value = ReservationNonWorkingTime.class)
    public ResponseEntity<Message> handleNonWorkingTime(ReservationNonWorkingTime exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(new Message(exception.getMessage()));
    }

    @ExceptionHandler(value = ReservationDurationInvalid.class)
    public ResponseEntity<Message> handleDurationInvalid(ReservationDurationInvalid exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new Message(exception.getMessage()));
    }

    @ExceptionHandler(value = ZoneIdInvalidException.class)
    public ResponseEntity<Message> handleZoneIdInvalid(ZoneIdInvalidException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new Message(exception.getMessage()));
    }
}
