package com.twuc.webApp.exception;

public class ZoneIdInvalidException extends RuntimeException {
    public ZoneIdInvalidException(String message) {
        super(message);
    }
}
