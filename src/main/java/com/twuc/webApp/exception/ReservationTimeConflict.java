package com.twuc.webApp.exception;

public class ReservationTimeConflict extends RuntimeException {
    public ReservationTimeConflict(String message) {
        super(message);
    }
}
