package com.twuc.webApp.exception;

public class ReservationDurationError extends RuntimeException {
    public ReservationDurationError(String message) {
        super(message);
    }
}
