package com.twuc.webApp.exception;

public class ReservationTimeInvalid extends RuntimeException {
    public ReservationTimeInvalid(String message) {
        super(message);
    }
}
