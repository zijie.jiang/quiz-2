package com.twuc.webApp.contract.staff;

import com.twuc.webApp.domain.staff.model.Staff;

public class ContractStaffResponse {

    private Long id;
    private String firstName;
    private String lastName;
    private String zoneId;

    public ContractStaffResponse() {
    }

    public ContractStaffResponse(Staff staff) {
        this.id = staff.getId();
        this.firstName = staff.getFirstName();
        this.lastName = staff.getLastName();
        this.zoneId = staff.getZone() == null ? null : staff.getZone().getZoneId();
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getZoneId() {
        return zoneId;
    }
}
