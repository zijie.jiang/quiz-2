package com.twuc.webApp.contract.staff;

import com.twuc.webApp.domain.staff.model.Staff;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ContractStaffRequest {

    private Long id;

    @NotBlank
    @Length(max = 64)
    private String firstName;

    @NotBlank
    @Length(max = 64)
    private String lastName;

    public ContractStaffRequest() {
    }

    public ContractStaffRequest(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Staff toStaff() {
        return new Staff(id, firstName, lastName);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
