package com.twuc.webApp.contract.zone;

import com.twuc.webApp.domain.zone.Zone;

import javax.validation.constraints.NotBlank;

public class ContractZoneRequest {

    @NotBlank
    private String zoneId;

    public ContractZoneRequest() {
    }

    public ContractZoneRequest(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public Zone toZone() {
        return new Zone(zoneId);
    }
}
