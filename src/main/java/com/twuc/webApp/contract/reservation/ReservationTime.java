package com.twuc.webApp.contract.reservation;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ReservationTime {
    private String zoneId;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime startTime;

    public ReservationTime() {
    }

    public ReservationTime(String zoneId, Instant startTime) {
        this.zoneId = zoneId;
        this.startTime = startTime.atZone(ZoneId.of(zoneId));
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }
}
