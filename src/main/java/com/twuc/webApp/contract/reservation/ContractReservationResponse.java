package com.twuc.webApp.contract.reservation;

import com.twuc.webApp.domain.reservation.model.Reservation;

import java.time.Duration;


public class ContractReservationResponse {
    private String username;

    private String companyName;

    private Duration duration;

    private ReservationStartTime startTime;

    public ContractReservationResponse() {
    }

    public ContractReservationResponse(Reservation reservation) {
        this.username = reservation.getUserName();
        this.companyName = reservation.getCompanyName();
        this.duration = Duration.ofHours(reservation.getDuration());
    }

    public ReservationStartTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ReservationStartTime startTime) {
        this.startTime = startTime;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Duration getDuration() {
        return duration;
    }
}
