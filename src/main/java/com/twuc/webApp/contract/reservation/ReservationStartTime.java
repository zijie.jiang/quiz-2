package com.twuc.webApp.contract.reservation;

public class ReservationStartTime {
    private ReservationTime client;
    private ReservationTime staff;

    public ReservationStartTime() {
    }

    public ReservationStartTime(ReservationTime client, ReservationTime staff) {
        this.client = client;
        this.staff = staff;
    }

    public ReservationTime getClient() {
        return client;
    }

    public ReservationTime getStaff() {
        return staff;
    }
}
