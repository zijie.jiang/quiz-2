package com.twuc.webApp.contract.reservation;

import com.twuc.webApp.domain.reservation.model.Reservation;
import org.hibernate.validator.constraints.Length;
import org.springframework.boot.convert.DurationFormat;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ContractReservationRequest {

    @NotBlank
    @Length(max = 128)
    private String username;

    @NotBlank
    @Length(max = 64)
    private String companyName;

    @NotNull
    private ZoneId zoneId;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime startTime;

    @NotNull
    @DurationFormat(value = DurationStyle.ISO8601)
    private Duration duration;

    public ContractReservationRequest() {
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public Reservation toReservation() {
        return new Reservation(this);
    }
}
