package com.twuc.webApp.domain.reservation.model;


import com.twuc.webApp.contract.reservation.ContractReservationRequest;
import com.twuc.webApp.domain.staff.model.Staff;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 128, nullable = false)
    private String userName;

    @Column(length = 64, nullable = false)
    private String companyName;

    private Long duration;

    private String zoneId;

    private Instant startTime;

    @ManyToOne
    private Staff staff;

    public Reservation() {
    }

    public Reservation(ContractReservationRequest contractReservationRequest) {
        this.userName = contractReservationRequest.getUsername();
        this.companyName = contractReservationRequest.getCompanyName();
        this.duration = contractReservationRequest.getDuration().toHours();
        this.zoneId = contractReservationRequest.getZoneId().toString();
        this.startTime = contractReservationRequest.getStartTime().toInstant();
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Long getDuration() {
        return duration;
    }

    public Staff getStaff() {
        return staff;
    }

    public String getZoneId() {
        return zoneId;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
}
