package com.twuc.webApp.domain.zone;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Zone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String zoneId;

    public Zone() {
    }

    public Zone(String zoneId) {
        this.zoneId = zoneId;
    }

    public Long getId() {
        return id;
    }

    public String getZoneId() {
        return zoneId;
    }
}
