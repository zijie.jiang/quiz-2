CREATE TABLE reservation (
	id bigint AUTO_INCREMENT PRIMARY KEY,
	company_name varchar(64) NOT NULL,
	duration varchar(255),
	start_time timestamp,
	user_name varchar(128) NOT NULL,
	zone_id varchar(255),
	staff_id bigint
);
