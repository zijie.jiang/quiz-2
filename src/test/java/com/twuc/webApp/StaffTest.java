package com.twuc.webApp;

import com.twuc.webApp.contract.staff.ContractStaffRequest;
import com.twuc.webApp.contract.zone.ContractZoneRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Objects;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class StaffTest extends ApiTestBase {
    @Test
    void should_return_201_and_contains_location_when_post_staff_rob_given_first_name_and_last_name() throws Exception {
        String serialize = serialize(new ContractStaffRequest("Rob", "Hall"));

        ResultActions resultActions = getSaveStaffResultActions(serialize);

        resultActions.andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/staffs/1"));
    }

    @Test
    void should_return_400_when_post_staff_rob_given_invalid_first_name_and_last_name() throws Exception {
        String serialize = serialize(new ContractStaffRequest(null, "Hall"));

        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_get_staff_rob_given_staff_id() throws Exception {
        String id = getStaffId();
        mockMvc.perform(get("/api/staffs/" + id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"));
    }

    @Test
    void should_return_404_when_get_staff_rob_given_not_exist_staff_id() throws Exception {
        long id = 333L;
        mockMvc.perform(get("/api/staffs/" + id))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_200_and_order_by_id_when_get_all_given_exist_rob_staffs() throws Exception {
        long expectId = 1L;
        getSaveStaffResultActions(serialize(new ContractStaffRequest("Rob", "Hall")));

        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(expectId))
                .andExpect(jsonPath("$[0].firstName").value("Rob"))
                .andExpect(jsonPath("$[0].lastName").value("Hall"));
    }

    @Test
    void should_return_empty_when_get_all_given_not_exist_rob_staffs() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    void should_return_200_when_put_rob_staff_zone_info_given_zone_id() throws Exception {
        ContractZoneRequest contractZoneRequest = new ContractZoneRequest("Asia/Chongqing");
        String serialize = serialize(contractZoneRequest);
        String id = getStaffId();
        putStaffZoneId(serialize, id)
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_put_rob_staff_zone_info_given_invalid_zone_id() throws Exception {
        String id = getStaffId();
        putStaffZoneId("{\"ZonEId\":\"Asia/Chongqing\"}", id)
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_and_contains_zone_info_when_get_rob_staff_given_staff_id() throws Exception {
        String staffId = getStaffId();
        ContractZoneRequest contractZoneRequest = new ContractZoneRequest("Asia/Chongqing");
        String serialize = serialize(contractZoneRequest);
        putStaffZoneId(serialize, staffId);

        mockMvc.perform(get("/api/staffs/" + staffId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(staffId))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_return_200_and_zone_id_is_null_when_get_rob_staff_given_staff_id() throws Exception {
        String staffId = getStaffId();

        mockMvc.perform(get("/api/staffs/" + staffId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(staffId))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"))
                .andExpect(jsonPath("$.zoneId").doesNotExist());
    }

    @Test
    void should_return_200_and_all_zone_id_when_get_all_zone_id() throws Exception {
        mockMvc.perform(get("/api/timezones"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0]").value("Africa/Abidjan"))
                .andExpect(jsonPath("$[1]").value("Africa/Accra"));
    }

    @Test
    void should_return_201_and_contains_location_when_post_reservation_given_staff_id_and_content() throws Exception {
        String staffId = getStaffId();
        putStaffZoneId(serialize(new ContractZoneRequest("Asia/Chongqing")), staffId);
        ResultActions resultActions = postReservation(staffId);
        resultActions.andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/staffs/" + staffId + "/reservations"));
    }

    @Test
    void should_return_400_when_post_reservation_given_invalid_content() throws Exception {
        MockReservationRequest reservationMockReservationRequest = new MockReservationRequest("Sofia",
                "ThoughtWorks",
                "Africa/Nairobi",
                "2019-10-20T11:46:00+0:00",
                "PT1H");
        String serialize = serialize(reservationMockReservationRequest);
        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_get_reservation_list_given_rob_staff_id() throws Exception {
        String staffId = getStaffId();
        ContractZoneRequest contractZoneRequest = new ContractZoneRequest("Asia/Chongqing");
        putStaffZoneId(serialize(contractZoneRequest), staffId);
        postReservation(staffId);

        mockMvc.perform(get("/api/staffs/" + staffId + "/reservations"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].username").value("Sofia"))
                .andExpect(jsonPath("$[0].companyName").value("ThoughtWorks"))
                .andExpect(jsonPath("$[0].duration").value("PT1H"))
                .andExpect(jsonPath("$[0].startTime.client.zoneId").value("Africa/Nairobi"))
                .andExpect(jsonPath("$[0].startTime.client.startTime").value("2019-10-20T08:46:00+03:00"))
                .andExpect(jsonPath("$[0].startTime.staff.zoneId").value("Asia/Chongqing"))
                .andExpect(jsonPath("$[0].startTime.staff.startTime").value("2019-10-20T13:46:00+08:00"));
    }

    @Test
    void should_return_409_and_contains_message_when_save_reservation_given_not_set_zone_id_rob_staff() throws Exception {
        String staffId = getStaffId();
        ResultActions resultActions = postReservation(staffId);

        resultActions.andExpect(status().is(409))
                .andExpect(jsonPath("$.message").value("{Rob Hall} is not qualified."));
    }

    @Test
    void should_return_400_and_contains_error_message_when_save_reservation_given_reservation_time_in_48_hours() throws Exception {
        String staffId = getStaffId();
        ContractZoneRequest contractZoneRequest = new ContractZoneRequest("Asia/Chongqing");
        putStaffZoneId(serialize(contractZoneRequest), staffId);
        MockReservationRequest reservationMockReservationRequest = new MockReservationRequest("Sofia",
                "ThoughtWorks",
                "Africa/Nairobi",
                "2019-08-20T11:46:00+03:00",
                "PT1H");
        String serialize = serialize(reservationMockReservationRequest);
        mockMvc.perform(post("/api/staffs/" + staffId + "/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message")
                        .value("Invalid time: the application is too close from now. The interval should be greater than 48 hours."));
    }

    @Test
    void should_return_409_and_contains_error_message_when_save_reservation_given_conflict_time() throws Exception {
        String staffId = getStaffId();
        ContractZoneRequest contractZoneRequest = new ContractZoneRequest("Asia/Chongqing");
        putStaffZoneId(serialize(contractZoneRequest), staffId);
        postReservation(staffId);
        ResultActions resultActions = postReservation(staffId);

        resultActions.andExpect(status().is(409))
                .andExpect(jsonPath("$.message").value("The application is conflict with existing application."));
    }

    @Test
    void should_return_409_and_contains_error_message_when_save_reservation_given_non_work_time() throws Exception {
        String staffId = getStaffId();
        ContractZoneRequest contractZoneRequest = new ContractZoneRequest("Asia/Chongqing");
        putStaffZoneId(serialize(contractZoneRequest), staffId);
        MockReservationRequest reservationMockReservationRequest = new MockReservationRequest("Sofia",
                "ThoughtWorks",
                "Africa/Nairobi",
                "2019-10-20T02:46:00+03:00",
                "PT1H");
        String serialize = serialize(reservationMockReservationRequest);
        mockMvc.perform(post("/api/staffs/" + staffId + "/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize))
                .andExpect(status().is(409))
                .andExpect(jsonPath("$.message")
                        .value("You know, our staff has their own life."));
    }

    @Test
    void should_return_400_and_contains_error_message_when_save_reservation_given_time_not_in_1_to_3() throws Exception {
        String staffId = getStaffId();
        ContractZoneRequest contractZoneRequest = new ContractZoneRequest("Asia/Chongqing");
        putStaffZoneId(serialize(contractZoneRequest), staffId);
        MockReservationRequest reservationMockReservationRequest = new MockReservationRequest("Sofia",
                "ThoughtWorks",
                "Africa/Nairobi",
                "2019-10-20T09:46:00+03:00",
                "PT5H");
        String serialize = serialize(reservationMockReservationRequest);
        mockMvc.perform(post("/api/staffs/" + staffId + "/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message")
                        .value("The application duration should be within 1-3 hours."));
    }

    private ResultActions postReservation(String staffId) throws Exception {
        MockReservationRequest reservationMockReservationRequest = new MockReservationRequest("Sofia",
                "ThoughtWorks",
                "Africa/Nairobi",
                "2019-10-20T08:46:00+03:00",
                "PT1H");
        String serialize = serialize(reservationMockReservationRequest);
        return mockMvc.perform(post("/api/staffs/" + staffId + "/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize));
    }

    private ResultActions putStaffZoneId(String serialize, String staffId) throws Exception {
        return mockMvc.perform(put("/api/staffs/" + staffId + "/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize));
    }

    private ResultActions getSaveStaffResultActions(String serialize) throws Exception {
        return mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize));
    }

    private String getStaffId() throws Exception {
        String serialize = serialize(new ContractStaffRequest("Rob", "Hall"));

        String location = getSaveStaffResultActions(serialize).andReturn().getResponse().getHeader("Location");
        String[] split = Objects.requireNonNull(location).split("/");
        return split[split.length - 1];
    }
}
