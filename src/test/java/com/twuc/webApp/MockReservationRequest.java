package com.twuc.webApp;

public class MockReservationRequest {
    private String username;

    private String companyName;

    private String zoneId;

    private String startTime;

    private String duration;

    public MockReservationRequest() {
    }

    public MockReservationRequest(String username, String companyName, String zoneId, String startTime, String duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }
}
